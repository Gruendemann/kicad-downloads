import json
from os.path import join, dirname
from jsonschema import validate
from typing import Any


def __get_json_schema(filename: str) -> Any:
    """ Loads the json schema file from tests/support/schema """

    relative_path = join('schemas', filename)
    absolute_path = join(dirname(__file__), relative_path)

    with open(absolute_path) as schema_file:
        return json.loads(schema_file.read())


def assert_valid_schema(json_data: Any, schema_file: str) -> None:
    """ Checks whether the given data matches the schema """

    schema = __get_json_schema(schema_file)

    # Throws exception if validation failure
    validate(instance=json_data, schema=schema)