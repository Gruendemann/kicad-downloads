aiofiles==0.7.0
Jinja2==3.0.1
uvicorn==0.14.0
itsdangerous==2.0.1
fastapi==0.67.0

# S3 Client (aws cli)
boto3==1.18.5

#redis
aioredis==2.0.0b1

#humanize
humanize==3.10.0