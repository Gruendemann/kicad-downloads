from fastapi import FastAPI, HTTPException
from fastapi.responses import JSONResponse, \
    PlainTextResponse, \
    RedirectResponse, \
    StreamingResponse, \
    FileResponse, \
    HTMLResponse, \
    Response
    
from fastapi.requests import Request

from operator import itemgetter
import urllib.parse

from app.models import *
from app.resources import templates
from app.config import KICAD_S3_ENDPOINT, KICAD_S3_BUCKET
import app.data
import app.s3

application = FastAPI()

@application.route('/')
async def get_kicad_index(request):
    platforms = app.data.platforms

    return templates.TemplateResponse("files/index.html", 
                                                    {
                                                        "request": request,
                                                        "platforms": platforms
                                                    })


@application.route('/{platform}')
async def get_platform_page(request: Request) -> Response:
    platform = request.path_params['platform']

    if platform not in app.data.platforms:
        raise HTTPException(status_code=404, detail="Unknown platform")

    platform_config = app.data.platforms[platform]

    return templates.TemplateResponse("files/platform.html", 
                                                    {
                                                        "request": request,
                                                        "platform_key": platform,
                                                        "platform": platform_config, 
                                                    })

    

@application.route('/{platform}/explore/{train}')
async def get_explore_view(request: Request) -> Response:
    platform = request.path_params['platform']
    train = request.path_params['train']

    if platform not in app.data.platforms:
        raise HTTPException(status_code=404, detail="Unknown platform")

    platform_config = app.data.platforms[platform]
    
    if train not in platform_config['explore']:
        raise HTTPException(status_code=404, detail="Unknown train")

    train_data = platform_config['explore'][ train ]

    files = app.s3.fetch_s3_objects(train_data['prefix'])

    files = files.values()
    files = sorted(files, key=itemgetter('last_modified'), reverse=True)

    return templates.TemplateResponse("files/explore.html", 
                                                    {
                                                        "request": request,
                                                        "platform_key": platform,
                                                        "platform": platform_config, 
                                                        "train_key": train,
                                                        "train": train_data,
                                                        "files": files,
                                                    })


@application.route('/{platform}/explore/{train}/download/{filename}')
async def get_explore_download(request: Request) -> Response:
    
    platform = request.path_params['platform']
    train = request.path_params['train']

    if platform not in app.data.platforms:
        raise HTTPException(status_code=404, detail="Unknown platform")

    platform_config = app.data.platforms[platform]
    
    if train not in platform_config['explore']:
        raise HTTPException(status_code=404, detail="Unknown train")

    train_data = platform_config['explore'][ train ]

    files = app.s3.fetch_s3_objects(train_data['prefix'])

    filename = request.path_params['filename']

    if filename not in files:
        raise HTTPException(status_code=404, detail="Unknown file")

    file = files[filename]
    url = urllib.parse.urljoin(KICAD_S3_ENDPOINT, KICAD_S3_BUCKET)
    url = urllib.parse.urljoin(url+"/", file['key'])

    return RedirectResponse(url, status_code=307)