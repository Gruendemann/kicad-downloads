from fastapi import FastAPI
from fastapi.responses import JSONResponse, \
    PlainTextResponse, \
    RedirectResponse, \
    StreamingResponse, \
    FileResponse, \
    HTMLResponse

from app.models import *

app = FastAPI()

@app.route('/')
async def index(request):
    content = []
    return JSONResponse(content)