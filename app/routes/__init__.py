from starlette.routing import Mount, Route, Router

from fastapi.responses import JSONResponse, \
    PlainTextResponse, \
    RedirectResponse
    

from .api import api_routes
from .kicad import kicad_routes

async def homebounce(request):
    url = request.url_for("get_kicad_index")
    return RedirectResponse(url)

# mounted app can also be an instance of `Router()`
routes = Router([
    Route("/", endpoint=homebounce),
    Mount('/api', app=api_routes),
    Mount('/kicad', app=kicad_routes),
])