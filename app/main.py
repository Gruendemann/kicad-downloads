from fastapi import FastAPI

from app.routes import routes
from app.resources import staticFiles, templates
import app.config as config
import app.data
import app.redis

application = FastAPI()
application.debug = config.DEBUG

application.mount('/static', staticFiles, name='static')
application.mount('/', routes)


@application.on_event('startup')
async def startup():
    await app.data.load()
    await app.redis.init()


@application.exception_handler(404)
async def not_found(request, exc):
    context = {"request": request}
    return templates.TemplateResponse("404.html", context, status_code=404)


@application.exception_handler(500)
async def server_error(request, exc):
    context = {"request": request}
    return templates.TemplateResponse("500.html", context, status_code=500)