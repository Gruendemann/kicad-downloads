import aioredis
from app.config import REDIS_CN

pool = None

async def init():
    global pool
    pool = await aioredis.from_url(REDIS_CN)