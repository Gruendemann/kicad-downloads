from datetime import datetime
from typing import TypedDict

class S3File(TypedDict):
    filename: str
    key: str
    last_modified: datetime 
    size: int