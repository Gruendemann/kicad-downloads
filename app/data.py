import aiofiles
import os
import json
import app.config as config


platforms = {}
mirrors = {}

async def __load_platforms():
    global platforms
    platforms = {}

    async with aiofiles.open(os.path.join(config.BASE_DIR, "data/platforms.json")) as f:
        contents = await f.read()
        
    platforms = json.loads(contents)

    return


async def __load_mirrors():
    global mirrors
    mirrors = {}

    async with aiofiles.open(os.path.join(config.BASE_DIR, "data/mirrors.json")) as f:
        contents = await f.read()

    mirrors = json.loads(contents)
    return

async def load():
    await __load_platforms()
    await __load_mirrors()
    return