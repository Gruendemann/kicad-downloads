from starlette.config import Config
from starlette.datastructures import URL, Secret
import os

# Config will be read from environment variables and/or ".env" files.
_config = Config(".env")

DEBUG = _config('DEBUG', cast=bool, default=False)
TESTING = _config('TESTING', cast=bool, default=False)
DATABASE_URL = _config('DATABASE_URL', default='')

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
BASE_DIR = os.path.dirname(APP_ROOT)


KICAD_S3_ENDPOINT = _config('KICAD_S3_ENDPOINT', default='https://s3.cern.ch/')
KICAD_S3_BUCKET = _config('KICAD_S3_BUCKET', default='kicad-downloads')

REDIS_CN = _config('REDIS_CN', default='redis://localhost')