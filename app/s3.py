import boto3
from botocore import UNSIGNED
from botocore.client import Config
from app.config import KICAD_S3_ENDPOINT, KICAD_S3_BUCKET
from app.models.s3 import S3File
from typing import Dict

s3_client = boto3.client('s3',
                config=Config(signature_version=UNSIGNED),
                endpoint_url=KICAD_S3_ENDPOINT)

def fetch_s3_objects(prefix) -> Dict[str, S3File]:
    response = s3_client.list_objects_v2(Bucket=KICAD_S3_BUCKET, Prefix=prefix)

    objects = {}
    if 'Contents' in response:
        for item in response['Contents']:
            filename = item['Key']
            filename = filename.replace(prefix,"")

            # a object can get accidentally created as the prefix only and end up an invalid file here so ignore it
            if filename:
                objects[filename] = S3File(filename=filename,
                                                key=item['Key'],
                                                last_modified=item['LastModified'], 
                                                size=item['Size'])

    return objects